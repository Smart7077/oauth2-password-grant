package com.smart.password.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.smart.password.domain.User;
import com.smart.password.domain.value.Role;
import com.smart.password.domain.value.UserStatus;
import com.smart.password.dto.LoggedUser;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class UserPrincipalDeserializer extends JsonDeserializer<LoggedUser> {

    @Override
    public LoggedUser deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        JsonNode jsonNode = mapper.readTree(jp);
        final JsonNode authoritiesNode = readJsonNode(jsonNode, "roles");
        final JsonNode userNode = readJsonNode(jsonNode, "user");
        Set<Role> userAuthorities = getUserAuthorities(mapper, authoritiesNode);

        Long id = readJsonNode(userNode, "id").asLong();
        String firstName = readJsonNode(userNode, "firstName").asText();
        String lastName = readJsonNode(userNode, "lastName").asText();
        String username = readJsonNode(userNode, "username").asText();
        String timezone = readJsonNode(userNode, "timezone").asText();
        String email = readJsonNode(userNode, "email").asText();
        String bio = readJsonNode(userNode, "email").asText();
        String phone = readJsonNode(userNode, "phone").asText();
        String statusInt = readJsonNode(userNode, "status").asText();
        UserStatus status = UserStatus.valueOf(statusInt);
        JsonNode passwordNode = readJsonNode(userNode, "password");
        String password = passwordNode.asText("");
        User currentUser = new User(id, firstName, lastName, username, email, phone, password, status, timezone, bio, userAuthorities);

        return new LoggedUser(currentUser);

    }

    private JsonNode readJsonNode(JsonNode jsonNode, String field) {
        return jsonNode.has(field) ? jsonNode.get(field) : MissingNode.getInstance();
    }

    private Set<Role> getUserAuthorities(final ObjectMapper mapper, final JsonNode authoritiesNode) throws JsonParseException, JsonMappingException, IOException {

        Set<Role> userAuthorities = new HashSet<>();
        if (authoritiesNode != null) {
            if (authoritiesNode.isArray()) {
                for (final JsonNode objNode : authoritiesNode) {
                    if (objNode.isArray()) {
                        ArrayNode arrayNode = (ArrayNode) objNode;
                        for (JsonNode elementNode : arrayNode) {
                            Role userAuthority = Role.fromString(mapper.readValue(elementNode.traverse(mapper), String.class));
                            userAuthorities.add(userAuthority);
                        }
                    }
                }
            }
        }
        return userAuthorities;
    }

}
