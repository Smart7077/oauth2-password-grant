//package com.smart.password.service.loggedUser;
//
//import com.jafton.rtc.core.app.domain.User;
//import com.jafton.rtc.core.app.dto.LoggedUser;
//import com.jafton.rtc.core.app.service.auth.AuthService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Map;
//
//@Service
//@RequiredArgsConstructor
//public class LoginAsService {
//
//    private final AuthService authService;
//
//    public Map<String, Object> loginAs(User user) {
//        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(user.getStringRoles());
//        LoggedUser loggedUser = new LoggedUser(user);
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loggedUser, null, authorityList);
//        return authService.createToken(authenticationToken);
//    }
//}
